import {Hero} from './hero';

export interface Team {
  id: number;
  name: string;
  heros: Array<Hero>;
  saved: boolean;
}
