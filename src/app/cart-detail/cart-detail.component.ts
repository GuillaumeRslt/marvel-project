import { Component, OnInit } from '@angular/core';
import {TeamManagerService} from '../services/team-manager/team-manager.service';
import {Team} from '../models/team';

@Component({
  selector: 'app-cart-detail',
  templateUrl: './cart-detail.component.html',
  styleUrls: ['./cart-detail.component.css']
})
export class CartDetailComponent implements OnInit {

  constructor(
    public teamManager: TeamManagerService
  ) { }

  ngOnInit(): void {
  }

  public saveTeam(): void {
    this.teamManager.saveTeam();
  }

  public deleteHero(id): void {
    this.teamManager.getCurrentTeam().saved = false;
    this.teamManager.getCurrentTeam().heros.splice(this.teamManager.getHeroIndexFromCurrentTeam(id), 1);
  }

  public team(): Team {
    return  this.teamManager.getCurrentTeam();
  }
}
