import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MarvelApiService} from '../services/marvel-api/marvel-api.service';
import {TeamManagerService} from '../services/team-manager/team-manager.service';
import {Hero} from '../models/hero';
import {Comic} from '../models/comic';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

  private _hero: Hero;
  private _firstComic: Comic;
  private _lastComic: Comic;

  constructor(
    public route: ActivatedRoute,
    public marvelApi: MarvelApiService,
    public teamManager: TeamManagerService
  ) { }

  ngOnInit(): void {
    const id: any = this.route.snapshot.paramMap.get('id');
    // Si les héros sont chargés on récupère en local
    if (this.marvelApi.getInit()) {
      this._hero = this.marvelApi.getHerosByIdLocal(id);
    } else {
      this.marvelApi.getHeroById(id).then(result => {
        this._hero = result;
      });
    }
    this.marvelApi.getFirstComicHero(id).then(result => {
      this._firstComic = result;
    });
    this.marvelApi.getLastComicHero(id).then(result => {
      this._lastComic = result;
    });
  }

  public getHero(): Hero {
    return this._hero;
  }
  public getFirstComic(): Comic {
    return this._firstComic;
  }
  public getLastComic(): Comic {
    return this._lastComic;
  }

  public isInTeam(id): boolean {
    return this.teamManager.getHeroIndexFromCurrentTeam(id) !== -1;
  }

  public addHeroToCurrentTeam(hero: Hero): void {
    this.teamManager.getCurrentTeam().saved = false;
    this.teamManager.getCurrentTeam().heros.push(hero);
  }
}
