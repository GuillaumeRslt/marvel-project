import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Hero} from '../../models/hero';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MarvelApiService {

  /**
   * indique si la liste des héros est chargée
   */
  private _init: boolean = false;
  /**
   * liste des héros suivant une recherche
   */
  private _heros: Array<Hero> = [];
  /**
   * Liste de tous les héros de l'API
   */
  private _allHeros: Array<Hero> = [];
  /**
   * Nombre de héros dans l'API
   */
  private _numberTotalOfHeros: number;

  constructor(private http: HttpClient) {
    this.getNumberTotalOfHeros().then(result => {
      this._numberTotalOfHeros = result;
      this.getAllHeros().then(() => {
          this._init = true;
      });
    });
  }

  public getInit(): boolean {
    return this._init;
  }

  public getHerosArray(): Array<any> {
    return this._heros;
  }

  private getNumberTotalOfHeros(): Promise<number> {
    return new Promise((resolve, reject) => {
      this.http.get('https://gateway.marvel.com:443/v1/public/characters?limit=1&apikey=' + environment.apiKey)
        .subscribe(result => {
          resolve(result['data']['total']);
        });
    });
  }

  private getOneRandomHero(): Hero {
    return this._allHeros[Math.floor(Math.random() * this._numberTotalOfHeros)];
  }

  public getTenRandomHeros(): void {
    this._heros = [];

    for (let i = 0; i < 10; i++) {
      const hero = this.getOneRandomHero();
      // on vérifie qu'il n'y ait pas 2 fois le même héros
      if (this._heros.includes(hero)) {
        i--;
      } else {
        this._heros.push(hero);
      }
    }
  }

  public getHerosByIdLocal(id): Hero {
    return this._allHeros.find(element => {
      return element.id === parseInt(id, 10);
    });
  }

  public async getHeroById(id): Promise<Hero> {
    return new Promise((resolve, reject) => {
      this.http.get('https://gateway.marvel.com:443/v1/public/characters/' + id + '?apikey=' + environment.apiKey).subscribe(result => {
        resolve(result['data']['results'][0]);
      });
    });
  }

  public async getAllHeros(): Promise<any> {
    return new Promise<void>((resolve, reject) => {
      for (let i = 0; i < this._numberTotalOfHeros; i += 100) {
        this.get100Heros(i).then(result => {
          result['data']['results'].forEach( dataResult => {
            this._allHeros.push({
              id: dataResult['id'],
              name: dataResult['name'],
              description: dataResult['description'],
              thumbnail: {
                path: dataResult['thumbnail']['path'],
                extension: dataResult['thumbnail']['extension']
              }
            });
          });
          if (this._allHeros.length === this._numberTotalOfHeros) {
            resolve();
          }
        });
      }
    });
  }

  private async get100Heros(offset): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.http.get('https://gateway.marvel.com:443/v1/public/characters?limit=100&offset=' + offset + '&apikey=' + environment.apiKey)
        .subscribe(result => {
        resolve(result);
      });
    });
  }

  /**
   * Regarde si "str" se trouve dans le nom ou la description des héros
   */
  public searchHeros(str: string): void {
    this._heros = [];
    str = str.toLowerCase();
    this._allHeros.forEach(hero => {
      if (hero.name.toLowerCase().search(str) !== -1 || hero.description.toLowerCase().search(str) !== -1) {
        this._heros.push(hero);
      }
    });
  }

  public getFirstComicHero(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get('https://gateway.marvel.com:443/v1/public/characters/' + id + '/comics?format=comic&orderBy=onsaleDate&limit=1&apikey=' + environment.apiKey)
        .subscribe(result => {
          resolve(result['data']['results'][0]);
        });
    });
  }

  public getLastComicHero(id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.get('https://gateway.marvel.com:443/v1/public/characters/' + id + '/comics?format=comic&orderBy=-onsaleDate&limit=1&apikey=' + environment.apiKey)
        .subscribe(result => {
          resolve(result['data']['results'][0]);
        });
    });
  }
}
