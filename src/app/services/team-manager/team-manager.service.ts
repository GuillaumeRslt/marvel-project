import {Injectable} from '@angular/core';
import {Team} from '../../models/team';
import {Hero} from '../../models/hero';

@Injectable({
  providedIn: 'root'
})
export class TeamManagerService {

  /**
   * blockSave... : Indique si la modal demandant de sauvegarder doit être affichée
   */
  public blockSaveSelect: number = null;
  public blockSaveNew: boolean = false;
  /**
   * Equipes sauvegardées sur le navigateur
   */
  public savedTeams: Array<Team> = [];
  /**
   * Equipe sélectionnée
   */
  private _currentTeam: Team;

  /**
   * On récupère ce qui est enregistré sur le navigateur
   * Sinon on créé une équipe vide
   */
  constructor() {
    const local = localStorage.getItem('teams');
    if (local !== null && local !== '[]') {

      this.savedTeams = JSON.parse(local);
      this._currentTeam = JSON.parse(JSON.stringify(this.savedTeams[0])); // deep copy

    } else {

      this._currentTeam = {
        id: this.createId(),
        name: 'Équipe 0',
        heros: Array(),
        saved: false
      };

    }
  }

  public getCurrentTeam(): Team {
    return this._currentTeam;
  }

  public renameCurrentTeam(newName): void {
    const str: string = newName;
    // on vérifie que le nom ne soit pas vide ou ne contienne que des espaces
    if (newName === '' || str.replace(/\s/g, '').length === 0) {
      document.querySelector('input.team-rename').classList.add('error');
    } else {
      this._currentTeam.saved = false;
      this._currentTeam.name = newName;
      document.querySelector('input.team-rename').classList.remove('error');
    }
  }

  public deleteHeroFromCurrentTeam(id): void {
    this._currentTeam.saved = false;
    this._currentTeam.heros.splice(this.getHeroIndexFromCurrentTeam(id), 1);
  }

  /**
   * Liste des équipes à afficher
   * On affiche la "currentTeam" qui a pu être modifiée
   * Et non celle qui est sauvegardée dans "savedTeams"
   * (utile pour voir nos modifications)
   */
  public getTeams(): Array<Team> {
    const indexCurrentTeam = this.getIndexFromSavedTeams(this._currentTeam.id);
    const teams: Array<Team> = [];
    teams.push(this._currentTeam);
    this.savedTeams.forEach((team, index) => {
      if (index !== indexCurrentTeam) {
        teams.push(team);
      }
    });

    return teams;
  }

  /**
   * Si tout est sauvegardé on ajoute directement une équipe
   * Sinon on affiche la demande de sauvegarde
   */
  public addNewTeamButton(): void {
    if (this._currentTeam.saved) {
      this.addNewTeam();
    } else {
      this.toggleSaveNew();
    }
  }

  public addNewTeam(): void {
    const newId = this.createId();
    this._currentTeam = {
      id: newId,
      name: 'Équipe ' + newId,
      heros: Array(),
      saved: false
    };
  }

  /**
   * Si tout est sauvegardé on sélectionne directement l'équipe
   * Sinon on affiche la demande de sauvegarde
   */
  public selectTeamButton(id): void {
    this.blockSaveSelect = id;
    if (this._currentTeam.saved) {
      this.selectTeam();
      this.blockSaveSelect = null;
    }
  }

  public selectTeam(): void {
    this._currentTeam = JSON.parse(JSON.stringify(this.savedTeams[this.getIndexFromSavedTeams(this.blockSaveSelect)])); // deep copy
    this.blockSaveSelect = null;
  }

  public saveTeam(): void {
    const index: number = this.getIndexFromSavedTeams(this._currentTeam.id);
    this._currentTeam.saved = true;
    if (index !== -1) {
      this.savedTeams[index] = this._currentTeam;
    } else {
      this.savedTeams.push(this._currentTeam);
    }
    localStorage.setItem('teams', JSON.stringify(this.savedTeams));
  }

  public deleteTeam(id: number): void {
    // cas spécifique : si on supprime la currentTeam mais qu'elle n'a jamais été sauvegardé
    if (this._currentTeam.id === id && this.getIndexFromSavedTeams(id) === -1) {
      if (this.savedTeams.length > 0) {
        this._currentTeam = JSON.parse(JSON.stringify(this.savedTeams[0]));
      } else { // si savedTeam est vide
        this.addNewTeam();
      }
    } else { // dans les autres cas
      if (this.savedTeams.length > 1) {
        this.savedTeams.splice(this.getIndexFromSavedTeams(id), 1);
        if (this._currentTeam.id === id) {
          this._currentTeam = this.savedTeams[0];
        }
      } else {
        this.savedTeams = [];
        this.addNewTeam();
      }
      localStorage.setItem('teams', JSON.stringify(this.savedTeams));
    }
  }

  /**
   * retourne un nouvel id
   */
  private createId(): number {
    let maxId = 0;
    if (this.savedTeams.length === 0) {
      return maxId;
    } else {
      this.savedTeams.forEach(team => {
        if (team.id >= maxId) {
          maxId = team.id + 1;
        }
      });

      return maxId;
    }
  }

  public getIndexFromSavedTeams(id): number {
    return this.savedTeams.findIndex(element => {
      return element.id === id;
    });
  }

  public getHeroIndexFromCurrentTeam(id): number {
    return this._currentTeam.heros.findIndex(element => {
      return element.id === id;
    });
  }

  public toggleSaveNew(): void {
    this.blockSaveNew = !this.blockSaveNew;
  }
}
