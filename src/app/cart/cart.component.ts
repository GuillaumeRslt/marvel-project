import { Component, OnInit } from '@angular/core';
import {TeamManagerService} from '../services/team-manager/team-manager.service';
import {Team} from '../models/team';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public modalVisible = false;

  constructor(
    public teamManager: TeamManagerService
  ) { }

  ngOnInit(): void {
  }

  /**
   * Faire apparaître ou non le détail de l'équipe
   */
  public toggleCartDetail(event): void {
    if (event.target.classList.contains('toggle')) {
      this.modalVisible = !this.modalVisible;
    }
  }

  public team(): Team {
    return  this.teamManager.getCurrentTeam();
  }
}
