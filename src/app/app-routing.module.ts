import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HerosListComponent} from './heros-list/heros-list.component';
import { HeroDetailComponent} from './hero-detail/hero-detail.component';
import {TeamsComponent} from './teams/teams.component';


const routes: Routes = [
  {path: 'home', component: HerosListComponent},
  {path: 'detail/:id', component: HeroDetailComponent},
  {path: 'teams', component: TeamsComponent},
  {path: '**', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
