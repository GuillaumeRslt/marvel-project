import { Component, OnInit } from '@angular/core';
import {TeamManagerService} from '../services/team-manager/team-manager.service';
/**
 * import de l'API
 * pas directement utilisée
 * Utile car charge tous les héros si on tombe directement sur cette page
 * Evite d'attendre le chargement quand on va ensuite sur l'accueil
 */
import {MarvelApiService} from '../services/marvel-api/marvel-api.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {

  constructor(
    public teamManager: TeamManagerService,
    public marvelApiService: MarvelApiService
  ) { }

  ngOnInit(): void {
  }
}
